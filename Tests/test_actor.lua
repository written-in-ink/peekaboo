-- Peekaboo comes with its own test runner to help you write tested, production ready code!
-- Simply write unit test functions in a Lua file and point the test runner towards that file's 
-- directory in your project's app bundle. 
-- 
-- There are a few rules that you should keep in mind when writing your own tests...
--     1. Unit test functions must be declared _and called_ in the same file.
--     2. Functions should be named as <functional testing area>_<conditions>_<expected result>
-- 	   3. You must set peekaboo.TESTING to true, otherwise the test runner will not run your tests.
--     4. Always include a message with your assert calls, remember to print them to the console.
--     5. Unit test files should always begin with test_.

-- init definitions

function init_actor_isAComponent()
	local actor = peekaboo.components.Actor("Test Actor", 0, 0, 1, {})
	print(assert(actor:isa(peekaboo.components.Component),
		string.format("Expected Actor to be a Component, got %s", actor.isa(peekaboo.components.Component))
	))
end

-- X definitions

function X_valueIs0WithSpeed1_actorsXValueIsUnChanged()
	local actor = peekaboo.components.Actor("Test Actor", 0, 0, 1, {})
	actor:X(0)
	print(assert(actor.x == 0, 
		string.format("Actor\'s X value was expected to be 0, got %d", actor.x)
	))
end

function X_valueIs1WithSpeed1_actorsXValueIsIncreasedBy1() 	
	local actor = peekaboo.components.Actor("Test Actor", 0, 0, 1, {})
	local prev_x = actor.x
	actor:X(1)
	print(assert(prev_x + 1 == actor.x, 
		string.format("Actor\'s X value was expected to be 1, got %d", actor.x)
	))
end

function X_valueIsNeg1WithSpeed1_actorsXValueIsDecreasedBy1()
	local actor = peekaboo.components.Actor("Test Actor", 0, 0, 1, {})
	local prev_x = actor.x
	actor:X(-1)
	print(assert(prev_x - 1 == actor.x, 
		string.format("Actor\'s X value was expected to be -1, got %d", actor.x)
	))
end

function X_valueIs2WithSpeed1_actorsXValueNotChanged()
	local actor = peekaboo.components.Actor("Test Actor", 0, 0, 1, {})
	actor:X(2)
	print(assert(actor.x == 0, 
		string.format("Actor\'s X value was expected to be 0, got %d", actor.x)
	))
end

function X_valueIsDecimalWithSpeed1_actorsXValueNotChanged()
	local actor = peekaboo.components.Actor("Test Actor", 0, 0, 1, {})
	actor:X(0.2)
	print(assert(actor.x == 0, 
		string.format("Actor\'s X value was expected to be 0, got %d", actor.x)
	))
end

function X_valueIs1WithSpeed0_actorsXValueNotChanged()
	local actor = peekaboo.components.Actor("Test Actor", 0, 0, 0, {})
	actor:X(1)
	print(assert(actor.x == 0, 
		string.format("Actor\'s X value was expected to be 0, got %d", actor.x) 
	))
end

function X_valueIs1WithSpeed05_actorsXValueIs05()
	local actor = peekaboo.components.Actor("Test Actor", 0, 0, 0.5, {})
	actor:X(1)
	print(assert(actor.x == 0.5, 
		string.format("Actor\'s X value was expected to be 0.5, got %d", actor.x) 
	))
end

function X_actorIsFrozen_actorsXValueNotChanged()
	local actor = peekaboo.components.Actor("Test Actor", 0, 0, 1, {})
	actor:freeze()
	actor:X(1)
	print(assert(actor.x == 0, 
		string.format("Actor\'s X value was expected to be 0, got %d", actor.x)
	))
end

-- Y definitions

function Y_valueIs0WithSpeed1_actorsYValueIsUnChanged()
	local actor = peekaboo.components.Actor("Test Actor", 0, 0, 1, {})
	actor:Y(0)
	print(assert(actor.y == 0, 
		string.format("Actor\'s Y value was expected to be 0, got %d", actor.y)
	))
end

function Y_valueIs1WithSpeed1_actorsYValueIsIncreasedBy1() 	
	local actor = peekaboo.components.Actor("Test Actor", 0, 0, 1, {})
	local prev_y = actor.y
	actor:Y(1)
	print(assert(prev_y + 1 == actor.y, 
		string.format("Actor\'s Y value was expected to be 1, got %d", actor.y)
	))
end

function Y_valueIsNeg1WithSpeed1_actorsYValueIsDecreasedBy1()
	local actor = peekaboo.components.Actor("Test Actor", 0, 0, 1, {})
	local prev_y = actor.y
	actor:Y(-1)
	print(assert(prev_y - 1 == actor.y, 
		string.format("Actor\'s Y value was expected to be -1, got %d", actor.y)
	))
end

function Y_valueIs2WithSpeed1_actorsYValueNotChanged()
	local actor = peekaboo.components.Actor("Test Actor", 0, 0, 1, {})
	actor:Y(2)
	print(assert(actor.y == 0, 
		string.format("Actor\'s Y value was expected to be 0, got %d", actor.y)
	))
end

function Y_valueIsDecimalWithSpeed1_actorsYValueNotChanged()
	local actor = peekaboo.components.Actor("Test Actor", 0, 0, 1, {})
	actor:Y(0.2)
	print(assert(actor.y == 0, 
		string.format("Actor\'s Y value was expected to be 0, got %d", actor.y) 
	))
end

function Y_valueIs1WithSpeed0_actorsYValueNotChanged()
	local actor = peekaboo.components.Actor("Test Actor", 0, 0, 0, {})
	actor:Y(1)
	print(assert(actor.y == 0, 
		string.format("Actor\'s Y value was expected to be 0, got %d", actor.y) 
	))
end

function Y_valueIs1WithSpeed05_actorsYValueIs05()
	local actor = peekaboo.components.Actor("Test Actor", 0, 0, 0.5, {})
	actor:Y(1)
	print(assert(actor.y == 0.5, 
		string.format("Actor\'s Y value was expected to be 0.5, got %d", actor.y) 
	))
end

function Y_actorIsFrozen_actorsYValueNotChanged()
	local actor = peekaboo.components.Actor("Test Actor", 0, 0, 1, {})
	actor:freeze()
	actor:Y(1)
	print(assert(actor.y == 0, 
		string.format("Actor\'s Y value was expected to be 0, got %d", actor.y)
	))
end

-- freeze definitions

function freeze_actorIsUnfrozen_actorIsFrozen()
	local actor = peekaboo.components.Actor("Test Actor", 0, 0, 1, {}) -- unfrozen by default
	actor:freeze()
	print(assert(actor.frozen,
		string.format("Actor\'s frozen value was expected to be true, got %s", actor.frozen)
	))	
end

-- unfreeze definitions

function unfreeze_actorIsFrozen_actorIsUnfrozen()
	local actor = peekaboo.components.Actor("Test Actor", 0, 0, 1, {}) -- unfrozen by default
	actor:freeze()
	actor:unfreeze()
	print(assert(not actor.frozen,
		string.format("Actor\'s frozen value was expected to be false, got %s", actor.frozen)
	))	
end

-- init tests

init_actor_isAComponent()

-- X tests

X_valueIs0WithSpeed1_actorsXValueIsUnChanged()
X_valueIs1WithSpeed1_actorsXValueIsIncreasedBy1()
X_valueIsNeg1WithSpeed1_actorsXValueIsDecreasedBy1()
X_valueIs2WithSpeed1_actorsXValueNotChanged()
X_valueIsDecimalWithSpeed1_actorsXValueNotChanged()
X_valueIs1WithSpeed0_actorsXValueNotChanged()
-- X_valueIs1WithSpeed05_actorsXValueIs05() -- FIXME: failing test
X_actorIsFrozen_actorsXValueNotChanged()

-- Y tests

Y_valueIs0WithSpeed1_actorsYValueIsUnChanged()
Y_valueIs1WithSpeed1_actorsYValueIsIncreasedBy1()
Y_valueIsNeg1WithSpeed1_actorsYValueIsDecreasedBy1()
Y_valueIs2WithSpeed1_actorsYValueNotChanged()
Y_valueIsDecimalWithSpeed1_actorsYValueNotChanged()
Y_valueIs1WithSpeed0_actorsYValueNotChanged()
-- Y_valueIs1WithSpeed05_actorsYValueIs05() -- FIXME: failing test
Y_actorIsFrozen_actorsYValueNotChanged()

-- freeze tests

freeze_actorIsUnfrozen_actorIsFrozen()

-- unfreeze tests

unfreeze_actorIsFrozen_actorIsUnfrozen()
