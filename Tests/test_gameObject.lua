-- init definitions

function init_gameObject_has6DigitId()
	local obj = peekaboo.GameObject({})	
	local string_id = tostring(obj.id)
	print(assert(#string_id == 6,
		string.format("GameObject\'s ID was expected to have 6 digits, got %d", #string_id)
	))
end

function init_create100000GameObjects_allHaveUniqueIDs() 
	local obj = peekaboo.GameObject({})	
	local count = 0
	local tmp_obj
	local duplicate = false
	local message = "GameObject\'s ID was unique after 100,000 other GameObjects were instantiated"
	while count < 100000 and not duplicate do
		count = count + 1	
		tmp_obj = peekaboo.GameObject({})	 
		if obj.id == tmp_obj.id then
			duplicate = true
			message = "GameObject\'s ID was not unique, expected it to be so"
		end
	end
		
	print(assert(not duplicate, message))	
end

function init_createGameObjectWithActorComponent_componentAddedToGameObject()
	local obj = peekaboo.GameObject({ 
		peekaboo.components.Actor("Actor", 10, 5, 1, {}),
	})
	
	print(assert(obj.components ~= nil,
		string.format("Expected GameObject\'s components to be not nil, got %s", obj.components)	
	))	
	
	print(assert(obj:getComponent("Actor") ~= nil,
		string.format("Expected GameObject\'s Actor to be not nil, got %s", obj:getComponent("Actor"))	
	))	
	
	print(assert(obj:getComponent("Actor").x == 10,
		string.format("Expected GameObject\'s Actor.x to be not 10, got %s", obj:getComponent("Actor").x)	
	)) 
	
	print(assert(obj:getComponent("Actor").gameObject == obj,
		string.format("Expected Actor\'s GameObject to be the same object, got %s", obj:getComponent("Actor").gameObject)	
	))
end

function init_createGameObjectWithMultipleComponents_componentsAddedToGameObject()
	local obj = peekaboo.GameObject({ 
		peekaboo.components.Actor("Actor", 0, 0, 1, {}),
		peekaboo.components.Component() })
	
	print(assert(obj.components ~= nil,
		string.format("Expected GameObject\'s components to be not nil, got %s", obj.components)	
	))	
	
	print(assert(obj:getComponent("Actor") ~= nil,
		string.format("Expected GameObject\'s Actor to be not nil, got %s", obj:getComponent("Actor"))	
	))	

	print(assert(obj:getComponent("Component") ~= nil,
		string.format("Expected GameObject\'s Component to be not nil, got %s", obj:getComponent("Component"))	
	))	

	print(assert(obj:getComponent("Actor") ~= obj:getComponent("Component"),
		string.format("Expected GameObject\'s first and second component to be different, got %s", 
					   obj:getComponent("Actor") ~= obj:getComponent("Component"))
	))
end

-- addComponent definitions

function addComponent_addSingleComponentAfterInstantiation_componentAddedToGameObject()
	local obj = peekaboo.GameObject({})
	obj:addComponent(	
		peekaboo.components.Actor("Actor", 0, 0, 1, {})
	)	
	
	print(assert(obj.components ~= nil,
		string.format("Expected GameObject\'s components to be not nil, got %s", obj.components)	
	))	

	print(assert(obj:getComponent("Actor") ~= nil,
		string.format("Expected GameObject\'s Actor to be not nil, got %s", obj:getComponent("Actor"))	
	))	
end

-- init tests

init_gameObject_has6DigitId()
init_create100000GameObjects_allHaveUniqueIDs()
init_createGameObjectWithActorComponent_componentAddedToGameObject()
init_createGameObjectWithMultipleComponents_componentsAddedToGameObject()

-- addComponent tests

addComponent_addSingleComponentAfterInstantiation_componentAddedToGameObject()
