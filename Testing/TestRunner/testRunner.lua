class('TestRunner', {
	testing_dir
}, peekaboo.testing).extends(Object)

function peekaboo.testing.TestRunner:init(testing_dir)	
	if string.sub(testing_dir, -1) == '/' then -- strip away trailing forward slash, if it exists
		testing_dir = string.sub(testing_dir, 1, #testing_dir - 1)
	end
	
	self.testing_dir = testing_dir	
end

function peekaboo.testing.TestRunner:run()
	if not peekaboo.TESTING then
		error('You must set the peekaboo.TESTING flag to true to run the Peekaboo test runner.')
	end

	local test_files = playdate.file.listFiles(self.testing_dir)
	
	if test_files == nil then
		error(string.format('Directory %s was not found anywhere in the project\'s app bundle.', self.testing_dir))
	end

	local system_path = ''	
	for _,file in pairs(test_files) do
		system_path = self.testing_dir .. "/" .. file
		print(string.format('Running test %s', system_path))
		playdate.file.run(system_path)
	end	
end
