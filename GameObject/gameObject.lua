class('GameObject', {
	id,
	components
}, peekaboo).extends(Object)

function peekaboo.GameObject:init(components) -- components: array of instantiated components 
	repeat	
		self.id = math.random(100000, 999999) 
	until (peekaboo.SCENE_GAMEOBJECTS[self.id] == nil)
	
	peekaboo.SCENE_GAMEOBJECTS[self.id] = self.className
	
	self.components = {}
	for _,component in ipairs(components) do
		self:addComponent(component)
	end
end

function peekaboo.GameObject:addComponent(component)
	if component:isa(peekaboo.components.Component) then
		component.gameObject = self
		self.components[component.className] = component
	else
		error("Attempted to add something that is not a valid component")		
	end	
end

function peekaboo.GameObject:getComponent(className) 
	return self.components[className]
end
