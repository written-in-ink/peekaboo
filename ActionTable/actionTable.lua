-- ActionTables are simply objects containing a table of sequential image tables
-- TODO: What is an Action Table _really_? Is it a component? Is it an object that supports a component?

class('ActionTable', {
	actions	-- table { 'actionName', 'imageTable' }
}).extends(Object)

function ActionTable:init(actions)
	self.actions = actions
end

function ActionTable:getAction(action)
	return self.actions[action]	
end
