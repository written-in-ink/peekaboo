-- Actor components define a game object which can move across the screen.
-- Actors have a defined x,y coordinate as well as a fixed speed. Actors may be physics enabled by
-- also attaching a Rigidbody component to the game object. An Actor with physics enabled may
-- have a variable speed based on the Rigidbody's velocity/friction.

-- TODO: Add Rigidbody component

class("Actor", {
	name,
	x,
	y, 
	speed,
	frozen -- determines whether an Actor can move
}, peekaboo.components).extends(peekaboo.components.Component)

function peekaboo.components.Actor:init(name, x, y, speed, spr_actions) 
	peekaboo.components.Actor.super:init(self)
	self.name = name
	self.x = x 	
	self.y = y
	self.speed = speed
	self.frozen = false 
end

local function isValidDirection(direction)
	if direction == 0 or direction == 1 or direction == -1 then
		return true
	end	

	if not peekaboo.TESTING then 	
		print('Warning! %d is not an integer between -1 and 1.', direction)
	end
	return false
end

function peekaboo.components.Actor:X(direction) 
	if self.frozen then return end
	if isValidDirection(direction) then
		self.x += direction * self.speed
	end		
end

function peekaboo.components.Actor:Y(direction) 
	if self.frozen then return end
	if isValidDirection(direction) then
		self.y += direction * self.speed
	end		
end

function peekaboo.components.Actor:update()
	error(string.format("You must implement '%s's update method in its subclass", self.name))
end

function peekaboo.components.Actor:freeze()
	self.frozen = true	
end

function peekaboo.components.Actor:unfreeze()
	self.frozen = false	
end
